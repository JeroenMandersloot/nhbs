import 'ol/ol.css';
import './assets/scss/reset.scss';
import './assets/scss/app.scss';

import Vue from 'vue';
import axios from 'axios';
import Color from 'color';

import {Map, View, Overlay} from 'ol';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import {fromLonLat} from 'ol/proj';

const API = 'http://localhost:5000/api';
const MAX_THRESHOLD = 10;

new Vue({
    el: '#app',
    data: function() {
        return {
            filterQuery: '',
            species: [],
            thresholds: ((n) => {
                const range = [];
                for (let i = 0; i <= n; i++) {
                    range.push(i);
                }
                return range;
            })(MAX_THRESHOLD),
        };
    },
    mounted: function() {
        axios.get(API + '/species').then(resp => { this.species = resp.data; });
    },
    filters: {
        lowercase: (s) => s.toLowerCase()
    },
    methods: {
        getSpeciesLocations: function() {
            const ids = [...document.getElementById('filter-species').querySelectorAll('input[type=checkbox]:checked')].map(node => parseInt(node.getAttribute('value')));
            const margin = parseInt(document.getElementById('filter-margin').querySelector('select').value);
            if (!ids.length) {
                map.clear();
            } else {
                axios.get(API + '/species/' + ids.join(',') + '/locations/' + margin)
                    .then(resp => {
                        map.clear();
                        for (const l of resp.data) {
                            map.addLocation(new Location(l.description, l.lat, l.lon, l.num_measurements, l.precision, l.recall, l.num_overlap, l.num_missing, l.num_observed));
                        }
                    });
            }
        },

        filterSpecies: function() {
            const species = document.getElementById('filter-species').querySelectorAll('li');
            const query = this.filterQuery.toLowerCase();
            for (const li of species) {
                if (li.querySelector('span.name').getAttribute('index').includes(query)) {
                    li.style.display = '';
                } else {
                    li.style.display = 'none';
                }
            }
        }
    }
});

const Location = function(description, lat, lon, num_measurements, precision, recall, num_overlap, num_missing, num_observed) {
    this.description = description;
    this.lat = lat;
    this.lon = lon;
    this.num_measurements = num_measurements;
    this.precision = precision;
    this.recall = recall;
    this.num_overlap = num_overlap;
    this.num_missing = num_missing;
    this.num_observed = num_observed;

    this.pos = () => fromLonLat([this.lon, this.lat]);
    this.score = () => 1.0 - this.num_missing / MAX_THRESHOLD;
    this.color = () => Color({h: this.score() * 240, s: 100, l: 50}).rgb().string();
    this.polygon = () => {
        const x = (Math.sin(2 * this.precision * Math.PI) + 1) / 2;
        const y = (1 - Math.cos(2 * this.precision * Math.PI)) / 2;
        let coords = [[0.5, 0], [0.5, 0.5], [x, y]];
        if (this.precision <= 0.5) {
            coords = coords.concat([[1, y], [1, 0]]);
        } else {
            coords = coords.concat([[0, y], [0, 1], [1, 1], [1, 0]]);
        }
        return 'polygon(' + coords.map(c => c[0] * 100 + '% ' + c[1] * 100 + '%').join(', ') + ')';
    };
};

const NHBSMap = function(target, pos) {
    this.locations = [];
    this.element = document.getElementById(target);

    // Initialize the map.
    this.map = new Map({
        target: target,
        layers: [new TileLayer({source: new OSM()})],
        view: new View({
          center: pos,
          zoom: 7
        })
    });

    // Initialize the popup with detailed location information.
    this.popupElement = document.getElementById('popup');
    this.popup = new Overlay({
        offset: [0, 30],
        element: this.popupElement,
        positioning: 'top-center'
    });
    this.map.addOverlay(this.popup);

    /**
     * Updates the information and position of the popup based on the given location.
     *
     * @param Location
     */
    this.updatePopup = function(location) {
        this.popupElement.querySelector('.location').innerHTML = location.description;
        this.popupElement.querySelector('.measurements').innerHTML = location.num_measurements;
        this.popupElement.querySelector('.num-overlap').innerHTML = location.num_overlap;
        this.popupElement.querySelector('.num-selected').innerHTML = location.num_overlap + location.num_missing;
        this.popupElement.querySelector('.num-observed').innerHTML = location.num_observed;
        this.popup.setPosition(location.pos());
        this.map.render();
    },

    /**
     * Adds a marker for the given location to the map.
     *
     * @param Location
     */
    this.addLocation = function(location) {
        const markerElement = document.createElement('div');
        markerElement.classList.add('marker');
        markerElement.style.backgroundColor = location.color();
        markerElement.addEventListener('click', () => {
            this.updatePopup(location);
            this.showPopup();
        });

        const pieElement = document.createElement('div');
        pieElement.classList.add('pie');
        pieElement.style.clipPath = location.polygon();
        markerElement.appendChild(pieElement);

        const marker = new Overlay({
            position: location.pos(),
            element: markerElement,
            positioning: 'center-center',
            stopEvent: false
        });

        this.map.addOverlay(marker);
        this.locations.push(marker);
    };

    /**
     * Removes all location markers from the map and hide the popup.
     */
    this.clear = function() {
        for (const location of this.locations) {
            this.map.removeOverlay(location);
        }
        this.locations = [];
        this.hidePopup();
    };

    // Simple utility functions.
    this.showPopup = () => this.popupElement.style.display = '';
    this.hidePopup = () => this.popupElement.style.display = 'none';
};

const map = new NHBSMap('map', fromLonLat([5.1214, 52.2907]));