import argparse
import csv
from datetime import datetime
from functools import wraps

from tqdm import tqdm

from backend.src.database.tables import Measurement, Species, Location, initialize_tables
from backend.src.util import get_session, get_engine, get_config


def memoize(func):
    """
    A decorator for functions whose return values should be memoized. In this context it is used to ensure that some
    functions that return an SqlAlchemy object does not create duplicate objects when passed the same data.

    :param func:
    :return:
    """

    # We use a dict to keep track of previously returned values, with the args and kwargs as key.
    func.cache = dict()

    @wraps(func)
    def wrapper(*args, **kwargs):
        # Make sure the args and kwargs are hashable, as we will be using them as a key.
        key = frozenset(set(args) | set(kwargs.items()))
        # If we haven't encountered these arguments before, compute the return value and store it.
        if key not in func.cache:
            func.cache[key] = func(*args, **kwargs)
        # Return the cached result.
        return func.cache[key]
    return wrapper


@memoize
def get_measurement_object(year, month, day, country_code, location):
    date = datetime(int(year), int(month), int(day)) if year and month and day else None
    return Measurement(date=date, country_code=country_code, location=location)


@memoize
def get_species_object(species):
    return Species(name=species) if species else None


@memoize
def get_location_object(lat, lon, description):
    return Location(lat=float(lat), lon=float(lon), description=description) if lat and lon and description else None


def parse_gbif_csv(session, path_to_csv):
    with open(path_to_csv) as csv_file:
        reader = csv.DictReader(csv_file, delimiter='\t')

        for row in tqdm(reader):
            # Replace all empty string values ('') by None.
            row = {k: v if v else None for k, v in row.items()}

            # Create or retrieve the Location object.
            location = get_location_object(lat=row['decimalLatitude'],
                                           lon=row['decimalLongitude'],
                                           description=row['locality'])

            # Create a Measurement object.
            measurement = get_measurement_object(year=row['year'],
                                                 month=row['month'],
                                                 day=row['day'],
                                                 country_code=row['countryCode'],
                                                 location=location)

            # Create or retrieve the Species object.
            species = get_species_object(species=row['species'])

            if location and species:
                if species not in measurement.species:
                    measurement.species.append(species)
                session.add(measurement)
    session.commit()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parse a GBIF .csv file and store the data in the database.')
    parser.add_argument('-f', '--file', action='store', default='latest', help='basename of the .csv file (no ext)')
    args = parser.parse_args()

    db = get_config().nhbs.database
    engine = get_engine(db.host, db.database, db.username, db.password)
    initialize_tables(engine, schema=db.schema)

    parse_gbif_csv(session=get_session(engine, schema=db.schema),
                   path_to_csv='resources/{}.csv'.format(args.file))
