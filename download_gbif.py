import os
import time
import zipfile

import requests
from requests.auth import HTTPBasicAuth

from backend.src.util import get_config

config = get_config()

download_key = None
base_url = '{}/{}'.format('http://api.gbif.org', config.nhbs.gbif.api_version)
auth = HTTPBasicAuth(username=config.nhbs.gbif.username, password=config.nhbs.gbif.password)

payload = {
    'creator': auth.username,
    'notification_address': [],
    'send_notification': False,
    'format': 'SIMPLE_CSV',
    'predicate': {
       'type': 'and',
       'predicates': [{
           'type': 'equals',
           'key': 'CLASS_KEY',
           'value': 7947184
        }, {
           'type': 'equals',
           'key': 'COUNTRY',
           'value': 'NL'
        }]
    }
}

try:
    headers = {'Content-Type': 'application/json'}
    url = base_url + '/occurrence/download/request'
    response = requests.post(url=url, json=payload, headers=headers, auth=auth)

    if response.status_code != 201:
        raise RuntimeError("Could not initiate download: {}".format(response.text))

    download_key = response.text
    print("Initiating download with code {}".format(download_key))

    poll_interval = 60
    while True:
        print("Polling status...")
        poll_url = 'http://api.gbif.org/v1/occurrence/download/request/{}'.format(download_key)
        response = requests.get(url=poll_url)
        if response.status_code == 404:
            print("Download is not ready yet, trying again in {} seconds".format(poll_interval))
            time.sleep(poll_interval)
        elif response.status_code == 200:
            print("Download is ready!")
            break
        else:
            raise RuntimeError('Error polling download status (response: {}, code: {})'.format(
                response.content,
                response.status_code))

    print("Downloading zip file...")
    download_url = base_url + '/occurrence/download/request/{}.zip'.format(download_key)
    r = requests.get(url=download_url, allow_redirects=True)
    download_path = 'resources/{}.zip'.format(download_key)
    open(download_path, 'wb').write(r.content)

    print("Extracting {}".format(download_path))
    with zipfile.ZipFile(download_path, 'r') as zf:
        zf.extractall('resources')

    file_path = 'resources/latest.csv'
    os.rename('resources/{}.csv'.format(download_key), file_path)
    print("File extracted to {}".format(file_path))
    
finally:
    if download_key:
        requests.delete(url=base_url + '/occurrence/download/request/{}'.format(download_key), auth=auth)
