import os

import matplotlib.pyplot as plt

from backend.src.util import get_session, get_config, get_engine

db = get_config().nhbs.database
engine = get_engine(db.host, db.database, db.username, db.password)
session = get_session(engine, db.schema)


def get_num_measurements_per_year():
    sql = '''
        WITH tmp AS (
            SELECT DATE_PART('year', date) AS year 
            FROM occurrence
        )
        SELECT year, COUNT(*)
        FROM tmp
        GROUP BY year
        HAVING year IS NOT NULL
        ORDER BY year ASC
    '''
    rows = session.execute(sql)
    data = []
    for row in rows:
        data.append((row[0], row[1]))
    return data


def get_cumulative_num_measurements_for_species():
    sql = '''
        SELECT COUNT(*) c
        FROM occurrence
        GROUP BY species_id
        HAVING species_id IS NOT NULL
        ORDER BY c ASC
    '''
    rows = session.execute(sql)
    data = []
    for i, row in enumerate(rows):
        data.append((i+1, row[0]))
    return data


def get_cumulative_num_locations_for_species():
    sql = '''
        WITH overview AS (
            SELECT DISTINCT species.id, species.name, location.lat, location.lon
            FROM species
                LEFT JOIN occurrence ON species.id = occurrence.species_id
                LEFT JOIN location ON location.id = occurrence.location_id
        )
        SELECT COUNT(*) num_locations 
        FROM overview 
        GROUP BY id, name
        ORDER BY num_locations ASC
    '''
    rows = session.execute(sql)
    data = []
    for i, row in enumerate(rows):
        data.append((i+1, row[0]))
    return data


def get_num_measurements_per_location():
    sql = '''
        SELECT COUNT(*)
        FROM location
            LEFT JOIN occurrence ON location.id = occurrence.location_id
        GROUP BY location.lat, location.lon
    '''
    rows = session.execute(sql)
    data = []
    for row in rows:
        data.append(row[0])
    return data


def plot_graph(data, xlabel, ylabel, fname):
    fig, ax = plt.subplots(1, 1)
    fig.set_size_inches((9, 6))
    fig.tight_layout()
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.plot(*zip(*data))
    plt.savefig(os.path.join('out', fname), bbox_inches='tight')


def plot_hist(data, xlabel, ylabel, fname, bins=10):
    fig, ax = plt.subplots(1, 1)
    fig.set_size_inches((9, 6))
    fig.tight_layout()
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.hist(data, bins=bins)
    plt.savefig(os.path.join('out', fname), bbox_inches='tight')


if __name__ == '__main__':
    plot_graph(get_num_measurements_per_year(), 'Year', '#Measurements', 'per_year.png')
    plot_graph(get_cumulative_num_measurements_for_species(), '#Species', '#Measurements', 'cum_measurements_for_species.png')
    plot_graph(get_cumulative_num_locations_for_species(), '#Species', '#Locations', 'cum_locations_for_species.png')
    plot_hist(get_num_measurements_per_location(), '#Measurements', '#Locations', 'distribution_measurements_per_location.png', 30)
