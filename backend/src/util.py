import confidence
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


def get_config():
    return confidence.load_name('database', 'config')


def get_engine(host, database, username, password):
    return create_engine('postgresql://{}:{}@{}/{}'.format(username, password, host, database))


def get_connection(engine, schema):
    return engine.connect().execution_options(schema_translate_map={None: schema})


def get_session(engine, schema):
    session_factory = sessionmaker(bind=engine)
    session_factory.close_all()
    session = session_factory()
    session.execute('SET search_path TO {}'.format(schema))
    return session


def create_schema(engine, schema):
    engine.execute('CREATE SCHEMA IF NOT EXISTS {}'.format(schema))
