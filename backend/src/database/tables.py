from sqlalchemy import Column, Integer, String, Float, Date, ForeignKey, Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

from backend.src.util import create_schema, get_connection, get_session

Base = declarative_base()


class MeasurementSpeciesLink(Base):
    __tablename__ = 'measurement_species'
    measurement_id = Column(Integer, ForeignKey('measurement.id'), primary_key=True)
    species_id = Column(Integer, ForeignKey('species.id'), primary_key=True)


class Measurement(Base):
    __tablename__ = 'measurement'
    id = Column(Integer, primary_key=True)
    date = Column(Date, nullable=True)
    country_code = Column(String(2))
    location_id = Column(Integer, ForeignKey('location.id'))

    location = relationship('Location', back_populates='measurements')
    species = relationship('Species', secondary=MeasurementSpeciesLink.__tablename__)


class Species(Base):
    __tablename__ = 'species'
    id = Column(Integer, primary_key=True)
    name = Column(String)

    measurements = relationship('Measurement', secondary=MeasurementSpeciesLink.__tablename__)


class Location(Base):
    __tablename__ = 'location'
    id = Column(Integer, primary_key=True)
    lat = Column(Float)
    lon = Column(Float)
    description = Column(String)

    measurements = relationship('Measurement', back_populates='location')


def initialize_tables(engine, schema):
    session = get_session(engine, schema)
    create_schema(engine, schema)
    connection = get_connection(engine, schema)
    Base.metadata.drop_all(connection)
    Base.metadata.create_all(connection)
    session.commit()

