"""
This file creates a local database in which the data can be stored. It is therefore only meant to be
used for local development (on Ubuntu).
"""

import subprocess

from backend.src.util import get_config


def create_local_database(database, username, password):
    commands = ["CREATE DATABASE {}".format(database),
                "CREATE USER {} WITH ENCRYPTED PASSWORD '{}'".format(username, password),
                "GRANT ALL PRIVILEGES ON DATABASE {} TO {}".format(database, username)]

    for command in commands:
        subprocess.call('sudo -u postgres psql -c "{};"'.format(command), shell=True)


if __name__ == '__main__':
    db = get_config().nhbs.database
    create_local_database(db.database, db.username, db.password)
