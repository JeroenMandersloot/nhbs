import re

from flask import Flask, jsonify
from flask_cors import CORS

from backend.src.util import get_session, get_config, get_engine

db = get_config().nhbs.database
engine = get_engine(db.host, db.database, db.username, db.password)
session = get_session(engine, db.schema)

app = Flask(__name__)
CORS(app)


@app.route('/api/species', methods=['GET'])
def get_species():
    query = '''
    WITH overview AS (
        SELECT DISTINCT species.id, species.name, location.lat, location.lon
        FROM species
            LEFT JOIN occurrence ON species.id = occurrence.species_id
            LEFT JOIN location ON location.id = occurrence.location_id)
    SELECT 
        id, 
        name,
        COUNT(*) num_locations 
    FROM overview 
    GROUP BY id, name
    ORDER BY num_locations DESC
    '''

    rows = session.execute(query)
    data = [{'id': row.id, 'name': row.name, 'num_locations': row.num_locations} for row in rows]
    return jsonify(data)


@app.route('/api/species/<species>/locations/', methods=['GET'], defaults={'margin': 1})
@app.route('/api/species/<species>/locations/<margin>', methods=['GET'])
def get_species_locations(species, margin):
    """
    Retrieves all relevant locations for the given set of `species`. The `margin` is used to compute a cutoff point:
    only locations that miss at most `margin` of the `species` are included in the results.

    :param species: str, comma-separated list of species ids
    :param margin: int, maximum number of missing species
    :return:
    """
    assert re.match(r'^(\d+,)*\d+$', species)
    query = '''
        WITH overview AS 
            (SELECT 
                location.lat, 
                location.lon,
                COUNT(occurrence.id) num_measurements,
                ARRAY_AGG(DISTINCT species.id) AS species,
                mode() WITHIN GROUP (ORDER BY location.description) AS description
            FROM location
                LEFT JOIN occurrence ON location.id = occurrence.location_id
                LEFT JOIN species ON species.id = occurrence.species_id
            WHERE species.id IS NOT NULL
            GROUP BY location.lat, location.lon)
        SELECT
            lat,
            lon,
            num_measurements,
            species,
            description,
            ARRAY_LENGTH(species, 1) num_observed,
            ARRAY_LENGTH(ARRAY(SELECT UNNEST(species) INTERSECT SELECT UNNEST(ARRAY[{0}])), 1) num_overlap
        FROM overview
    '''.format(species)

    rows = session.execute(query)
    found = set(map(int, species.split(',')))
    num_found = len(found)
    margin = int(margin)
    data = [{'lat': row.lat,
             'lon': row.lon,
             'species': row.species,
             'description': row.description,
             'precision': row.num_overlap / row.num_observed,
             'recall': row.num_overlap / num_found,
             'num_overlap': row.num_overlap,
             'num_missing': num_found - row.num_overlap,
             'num_observed': row.num_observed,
             'num_measurements': row.num_measurements}
            for row in rows if row.num_overlap and num_found - row.num_overlap <= margin]
    return jsonify(data)


@app.route('/api/locations', methods=['GET'])
def get_locations():
    query = '''
        WITH overview AS (
            SELECT location.lat, location.lon, COUNT(DISTINCT occurrence.date)::float amount
            FROM occurrence
            LEFT JOIN location
                ON location.id = occurrence.location_id
            GROUP BY location.lat, location.lon
        )
        SELECT lat, lon, amount / (SELECT MAX(amount) FROM overview) score 
        FROM overview
        ORDER BY amount DESC
    '''

    rows = session.execute(query)
    data = [{'lat': row.lat, 'lon': row.lon, 'score': row.score} for row in rows]
    return jsonify(data)


def main():
    app.run(host="0.0.0.0", port=5000, debug=True)


if __name__ == '__main__':
    main()
