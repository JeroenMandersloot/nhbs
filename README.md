# Getting started

First, install the required dependencies for both the Python code and the frontend.
```bash
$ pip install -r requirements.txt
$ npm install --prefix frontend
```

Make sure all the required configuration properties are in place. In your local `config.yaml` file, 
define the following options:
```bash
nhbs.gbif.username
nhbs.gbif.password
nhbs.database.username
nhbs.database.password
nhbs.database.schema
nhbs.database.host
```
Other, non-personal configuration options are defined in this repository's `config.yaml` file and 
should generally not be modified.

Then, if you're working locally with a database on your own machine, make sure PostgreSQL is 
installed and that your database is configured correctly. You can do this with the commands below,
though these only work on Ubuntu. For other operating systems, you can install PostgreSQL and 
configure the database manually.
```bash
$ sudo apt-get update
$ sudo apt-get install postgresql postgresql-contrib
$ python setup_local_database.py
```

Next, if you're working on a new schema and you haven't loaded the data yet, you need to download the data from GBIF.
For this you need to create an account on <https://www.gbif.org>. Add your user credentials to your personal config file
as `nhbs.gbif.username` and `nhbs.gbif.password`. You can get then download the latest GBIF data as follows:
```bash
$ python download_gbif.py
$ python parse_gbif.py
```
Both of these scripts may take a while. Finally, run both the backend and frontend servers to start the web application.
```bash
$ python run_server.py
$ npm start --prefix frontend
```